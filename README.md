# mkdocs

This repo build and publish the Mkdocs container image including the theme and extensions.


## DEPRECATED

The image [epfl-si/mkdocs-material](https://github.com/epfl-si/mkdocs-material) has replaced this repository.


## CI

To build and publish the image, this repository use the Gitlab CI/CD. Look inside *.gitlab-ci.yml* for details.


## Theme

[Mkdocs-material](https://squidfunk.github.io/mkdocs-material/)


## Extensions

* [plantuml-markdown](https://github.com/mikitex70/plantuml-markdown)
* [mkdocs-git-revision-date-localized-plugin](https://github.com/timvink/mkdocs-git-revision-date-localized-plugin)
* [pymdown-extensions](https://github.com/facelessuser/pymdown-extensions/)

The gitlab CI will publish the image into our Container Registry.

## How to use extensions in your mkdocs.yml

```yaml
# Extensions
markdown_extensions:
  - admonition
  - sane_lists
  - codehilite
  - pymdownx.superfences
  - pymdownx.tabbed
  - pymdownx.tilde
  - pymdownx.details
  - plantuml_markdown:
      server: http://www.plantuml.com

plugins:
  - search
  - git-revision-date-localized
```

## Build site (.md -> .html)

Open a terminal inside the folder where is located your *mkdocs.yml* file, then:

TODO : Fix the registry url when available!

```bash
# Docker
sudo docker run --rm -v $PWD:/app:rw --name mkdocs-builder siexheb/mkdocs:2020-02-21d build

# Podman over Selinux
docker run --rm -v $PWD:/app:rw --security-opt label=disable --name mkdocs siexheb/mkdocs:2020-02-21d build
```
This will compile your site into $PWD/site

