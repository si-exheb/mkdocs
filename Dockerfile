ARG PYTHON_VER
FROM python:${PYTHON_VER}
LABEL maintainer="laurent.indermuehle@epfl.ch"
ARG MKDOCS_VER
ARG DEBIAN_FRONTEND=noninteractive
WORKDIR /app

# URL of dependencies releases:
# mkdocs-material:        https://github.com/squidfunk/mkdocs-material/releases
# plantuml-markdown:      https://github.com/mikitex70/plantuml-markdown/releases
# [...]-localized-plugin: https://github.com/timvink/mkdocs-git-revision-date-localized-plugin/releases
# pymdown-extensions:     https://github.com/facelessuser/pymdown-extensions/releases

# Libsqlit3-0 was removed due to CVE-2019-20218 and CVE-2019-19603

RUN apt-get update                                       \
    && apt-get remove -y libsqlite3-0                    \
    && apt-get install -y --no-install-recommends git    \
    && apt-get clean                                     \
    && rm -rf /var/lib/apt/lists/*                       \
    && pip install --no-cache-dir                        \
    "mkdocs==${MKDOCS_VER}"                              \
    'mkdocs-material==5.5.12'                            \
    'plantuml-markdown==3.4.0'                           \
    'mkdocs-git-revision-date-localized-plugin==0.7.1'   \
    'pymdown-extensions==8.0'                            \
    && rm -rf /tmp/*
VOLUME /app
EXPOSE 8000
ENTRYPOINT ["mkdocs"]
CMD ["serve", "--dev-addr=localhost:8000"]
