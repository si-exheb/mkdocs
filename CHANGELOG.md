* 2020-03-26
    * Update
        * Upgrade to plantuml-markdown 3.2.2
        * Upgrade to mkdocs-git-revision-date-localized-plugin 0.4.8
    * Change
        * Remove markdown-include plugin. It was useless because it don't manage relative path of included pages.